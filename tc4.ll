; ModuleID = '<stdin>'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: nounwind uwtable
define i32 @pie() #0 {
entry:
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store i32 9, i32* %x, align 4
  %call = call i32 (i8*, ...)* @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8]* @.str, i64 0, i64 0), i32* %y) #2
  %call1 = call i32 (i8*, ...)* @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8]* @.str, i64 0, i64 0), i32* %x) #2
  %0 = load i32* %y, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %x, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  switch i32 %0, label %sw.epilog [
    i32 1, label %sw.epilog.thread3
    i32 2, label %sw.epilog.thread
  ]

sw.epilog.thread3:                                ; preds = %if.end
  store i32 5, i32* %x, align 4
  br label %if.end6

sw.epilog.thread:                                 ; preds = %if.end
  store i32 7, i32* %x, align 4
  br label %if.then5

sw.epilog:                                        ; preds = %if.end
  %.pr1 = load i32* %x, align 4
  %cmp4 = icmp sgt i32 %.pr1, 6
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %sw.epilog.thread, %sw.epilog
  store i32 8, i32* %x, align 4
  br label %if.end6

if.end6:                                          ; preds = %sw.epilog.thread3, %if.then5, %sw.epilog
  %1 = phi i32 [ 5, %sw.epilog.thread3 ], [ 8, %if.then5 ], [ %.pr1, %sw.epilog ]
  %call7 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([3 x i8]* @.str, i64 0, i64 0), i32 %1) #2
  %2 = load i32* %x, align 4
  ret i32 %2
}

; Function Attrs: nounwind
declare i32 @__isoc99_scanf(i8* nocapture readonly, ...) #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) #1

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = metadata !{metadata !"clang version 3.5.1 (tags/RELEASE_351/final)"}
